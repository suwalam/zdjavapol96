package pl.sda.mockito.message;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
class PrivateMessageSenderTest {

    private static final String MESSAGE_TEXT = "Hello Andrzej";

    private static final String AUTHOR_ID = "Michał";

    private static final String RECIPIENT_ID = "Andrzej";

    @Mock
    private MessageProvider messageProvider;

    @Mock
    private MessageValidator messageValidator;

    @InjectMocks
    private PrivateMessageSender underTest;

    @Captor
    private ArgumentCaptor<Message> messageCaptor;

    @Test
    public void shouldSendPrivateMessage() {
        //given
        Mockito.when(messageValidator.isMessageValid(any())).thenReturn(true);
        Mockito.when(messageValidator.isMessageRecipientReachable(RECIPIENT_ID)).thenReturn(true);
        Mockito.doNothing().when(messageProvider).send(any(Message.class), eq(MessageType.PRIVATE));

        //when
        underTest.sendPrivateMessage(MESSAGE_TEXT, AUTHOR_ID, RECIPIENT_ID);

        //then
        Mockito.verify(messageValidator).isMessageValid(any());
        Mockito.verify(messageValidator).isMessageRecipientReachable(RECIPIENT_ID);
        Mockito.verifyNoMoreInteractions(messageValidator);

        Mockito.verify(messageProvider).send(any(), eq(MessageType.PRIVATE));
        Mockito.verifyNoMoreInteractions(messageProvider);
    }




    @Test
    public void shouldSendPrivateMessageUsingArgumentCaptor() {
        //given
        Mockito.when(messageValidator.isMessageValid(any())).thenReturn(true);
        Mockito.when(messageValidator.isMessageRecipientReachable(RECIPIENT_ID)).thenReturn(true);
        Mockito.doNothing().when(messageProvider).send(any(Message.class), eq(MessageType.PRIVATE));

        //when
        underTest.sendPrivateMessage(MESSAGE_TEXT, AUTHOR_ID, RECIPIENT_ID);

        //then
        Mockito.verify(messageValidator).isMessageValid(messageCaptor.capture());
        Mockito.verify(messageValidator).isMessageRecipientReachable(RECIPIENT_ID);
        Mockito.verifyNoMoreInteractions(messageValidator);

        Mockito.verify(messageProvider).send(messageCaptor.capture(), eq(MessageType.PRIVATE));
        Mockito.verifyNoMoreInteractions(messageProvider);

        final List<Message> messageCaptorAllValues = messageCaptor.getAllValues();

        for (Message message : messageCaptorAllValues) {
            Assertions.assertThat(message).isNotNull();
            Assertions.assertThat(message.getAuthor()).isEqualTo(AUTHOR_ID);
            Assertions.assertThat(message.getRecipient()).isEqualTo(RECIPIENT_ID);
            Assertions.assertThat(message.getValue()).isEqualTo(MESSAGE_TEXT);
            Assertions.assertThat(message.getSendAt()).isBefore(LocalDateTime.now());
            Assertions.assertThat(message.getId()).isNotNull();
        }
    }

    //praca domowa
    //przypadek negatywny

}