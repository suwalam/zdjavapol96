package pl.sda.mockito.database;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DataServiceImplTest {

    private static final String DATA_ELEMENT = "data element";

    private static final Data TEST_DATA = new Data.DataBuilder().value(DATA_ELEMENT).build();

    @Mock
    private DatabaseConnection databaseConnection;

    @Spy
    private DataRepository dataRepository = new DataRepositoryImpl();

    @InjectMocks
    private DataServiceImpl underTest;

    @Test
    public void shouldAddDataWhenDatabaseConnectionIsOpened() {
        //given
        Mockito.when(databaseConnection.isOpened()).thenReturn(false, true);

        //when
        final Data result = underTest.add(TEST_DATA);

        //then
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.getId()).isNotNull();
        Assertions.assertThat(result.getValue()).isEqualTo(DATA_ELEMENT);
        Mockito.verify(databaseConnection, Mockito.times(3)).isOpened();
        Mockito.verify(databaseConnection).open();
        Mockito.verify(databaseConnection).close();
        Mockito.verifyNoMoreInteractions(databaseConnection);

        Mockito.verify(dataRepository).add(TEST_DATA);
        Mockito.verifyNoMoreInteractions(dataRepository);
    }

    //praca domowa - kolejne przypadki metody add

}