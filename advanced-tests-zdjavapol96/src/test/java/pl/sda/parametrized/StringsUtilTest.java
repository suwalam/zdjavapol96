package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.junit.jupiter.api.Assertions.*;

class StringsUtilTest {

    @ParameterizedTest
    //given
    @CsvSource({"   test  ,TEST", "test   ,TEST", "TEst,TEST"})
    public void shouldTrimAndUpperCaseInput(String input, String expected) {
       //when
        final String result = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, result);
    }

    @ParameterizedTest
    //given
    @CsvSource(value = {"   test  ;TEST", "test   ;TEST", "TEst;TEST"}, delimiter = ';')
    public void shouldTrimAndUpperCaseInputWithCustomDelimiter(String input, String expected) {
        //when
        final String result = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, result);
    }


    @ParameterizedTest
    //given
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1, delimiter = ',', lineSeparator = ";")
    public void shouldTrimAndUpperCaseInputUsingCSVFileSource(String input, String expected) {
        //when
        final String result = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, result);
    }


    @ParameterizedTest
    @NullAndEmptySource
    public void shouldBeBlankOrNull(String input) {
        final boolean result = StringsUtil.isBlank(input);
        assertTrue(result);
    }



}