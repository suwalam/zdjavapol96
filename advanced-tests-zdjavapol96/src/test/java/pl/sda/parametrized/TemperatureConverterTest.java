package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class TemperatureConverterTest {

    @ParameterizedTest
    @EnumSource(value = TemperatureConverter.class, names = {"KELVIN_CELSIUS"}, mode = EnumSource.Mode.EXCLUDE)
    public void shouldConvertTempToValueHigherThanAbsoluteZero(TemperatureConverter converter) {
        //when
        final float result = converter.convertTemp(-273.15f);
        assertTrue(result >= -460);
    }

}