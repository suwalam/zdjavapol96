package pl.sda.parametrized;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NumbersUtilTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 87, 15})
    public void shouldReturnTrueForOddNumbers(int input) {
        //when //then
        assertTrue(NumbersUtil.isOdd(input));
    }

    //Praca domowa
    //przetestować metodę isOdd dla liczb parzystych wykorzystując adnotację @ValueSource

    @ParameterizedTest
    @MethodSource("provideNumbersWithParity")
    public void shouldReturnExpectedValueForGivenInput(int input, boolean expected) {
        assertEquals(expected, NumbersUtil.isOdd(input));
    }

    private static Stream<Arguments> provideNumbersWithParity() {
        return Stream.of(
                Arguments.of(2, false),
                Arguments.of(21, true),
                Arguments.of(1, true),
                Arguments.of(-2, false)
        );
    }

    @ParameterizedTest
    @ArgumentsSource(NumberWithParityProvider.class)
    public void shouldReturnExpectedValueForGivenInputUsingArgumentsSource(int input, boolean expected) {
        assertEquals(expected, NumbersUtil.isOdd(input));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenDivideByZero() {
        Assertions
                .assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> NumbersUtil.divide(10, 0))
                .withMessage("dividend can't be 0");
    }
    //praca domowa
    //przetestować metodę divide różnymi adnotacjami parametryzowanymi
}